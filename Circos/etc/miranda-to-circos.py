#!/bin/evn python
from __future__ import division
import re
import sys

def main(argv):
    direction_file = open("etc/circos-linkends.txt","w")
    results_file = open("etc/circos_links.txt", "w")
    #open results file to write to
    with open(argv[1],"r") as myfile:
        miranda_file = myfile.read()
        matches = re.findall(">.*", miranda_file)
    #print (matches)
    #open and read in the miranda output, then use regular expressions to just extract the matches
    
    for i in range(len(matches)):
        try:
            query_positions= re.findall("\|\d+", matches[i])
            current_match = matches[i]
            if current_match[1]== ">":
                pass
                #skip match if its the summary one

            else:
                query_start_pos=query_positions[-2]
                query_end_pos= query_positions[-1]
                query_start = query_start_pos[1:]
                query_end = query_end_pos[1:]#remove the pipe symbol and keep all the didgets after it
                query_pos= str(query_start) + " " + str(query_end) 
                
                #######################
                #######################
                ###PLACEHOLDER TEXT####
                #####CHANGE ME!!!!#####
                query_name = "III"
                #needs to extract the name of the chromosome that the miRNA came from#
                #######################
                #######################
                #######################
                #######################
                

                
                #query_start_pos=query_positions[0]
                #query_end_pos= query_positions[1]
                #if (int(query_end_pos[1:])-int(query_start_pos[1:])>20): #if the mature miRNA is less than 20 nucleotides then get rid of it. NB this filtering step is now taken care of much earlier.
                ref_positions = re.findall("\\t\d+\s\d+", current_match)
                ref_pos = ref_positions[1]
                ref_pos = ref_pos[1:]
                ref_start= ref_pos.split(" ")[0]
                ref_end= ref_pos.split(" ")[1]
                split_match = current_match.split("\t")#split up the name by tabs
                ref_name = split_match[1]#the second one is the name of the reference query
                #print (ref_name)
                if ref_pos == query_pos:
                    print ("Match "+str(i)+" is a self match. Moving on.")
                elif (ref_start<query_start<ref_end) or (ref_start<query_end<ref_end):
                    print ("Match "+str(i)+" is an overlapping match. Moving on.")
    
                else:
                    results_file.write("link"+str(i+1)+" "+query_name+" "+query_start+" "+query_end+ "\n")
                    results_file.write("link"+str(i+1)+" "+ref_name+" "+ref_pos+"\n")
                    #direction_file.write("link"+str(i+1)+" III "+query_start+" "+query_end+ " 0\n")
                    direction_file.write(ref_name+" "+ref_pos+" 0 \n")
                
        except IndexError:
            print ("Naming convention wrong for match " + str(i)+". Moving onto the next one.")
            
    results_file.close()
    direction_file.close()
    print("Links extracted successfully.")
    #print (str(i)+ " links found.")
    
if __name__ == "__main__":
	main(sys.argv)

