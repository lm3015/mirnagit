#!/bin/evn python

import re
import sys

def main(argv):
    with open(argv[1], 'r') as myfile:
        genomefile = myfile.read()
        names = re.findall('>.*',genomefile)
        #extract the names of all the chromosomes as the file is being read in
    
        genomefile = genomefile.replace('\n','') #get rid of newline characters

        chromosomes = re.split(">",genomefile)
        chromosomes = list(filter(None,chromosomes))

        name_lengths = []
        for name in names:
            name_lengths.append(len(name)-1)

            # just_seqs = []
        chromosome_lengths=[]
        text_file = open("etc/circos_ideograms.txt", "w")
        for i in range(len(chromosomes)):
            chromosome = chromosomes[i]
            just_seq = chromosome[name_lengths[i]:]
            #just_seqs.append(just_seq)
            chromosome_lengths.append(len(just_seq)) 
            current_name = names[i]
            text_file.write("chr - "+current_name[1:]+" "+str(i+1)+" 0 "+str(len(just_seq))+" spectral-"+str(len(names))+"-div-"+str(i+1)+"\n")

        #mito_file = open("C.elegans_mt.fasta", "w")
        #mito_file.write(name+" "+just_seq)
        
       
                #print (chromosome_lengths)
    #extracts the length of each chromosome minus the length of its name
    #if the two just_seqs lines are uncomments they will store the actual sequence of each chromosome without the names


    text_file.close()
    #mito_file.close()
    message = str(i+1)+" chromosomes found and written to file."
    print (message)
    
if __name__ == "__main__":
	main(sys.argv)



    
    
