#!/bin/evn python
from __future__ import division
import re
import sys
import os

###Run all the circos processes. Requires miranda output in the same folder as well as the two scripts (fasta-to-circos.py and miranda-to-circos.py) be available. Requires the circos-conf folder in the working directory.###


def main(argv):
    miranda_file = argv[1]
    try:
        os.system("python /project/data/amoeba/Code/mysite/synteny/etc/miranda-to-circos.py "+miranda_file +" &> /project/data/amoeba/Code/mysite/synteny/miranda-to-circos-log.txt")
        print("miranda-to-circos run. Output is in /project/data/amoeba/Code/mysite/synteny/miranda-to-circos-log.txt")
    except:
        print("Error, miranda-to-circos.py not found. Is the etc folder with the script in it in /project/data/amoeba/Code/mysite/synteny??")
    
    with open(miranda_file,"r") as myfile:
        miranda_file = myfile.read()
    genome_file = re.findall("Reference Filename:.*", miranda_file)
    genome_file = genome_file[0]
    genome_file = genome_file[20:]
    #print(genome_file)
    #finds the path to the file that miranda was given as the genome so it can be used by to produce an ideogram for circos
    try:
        os.system("python /project/data/amoeba/Code/mysite/synteny/etc/fasta-to-circos.py etc/"+ genome_file+ " &> /project/data/amoeba/Code/mysite/synteny/fasta-to-circos-log.txt")
        print("fasta-to-circos run. Output is in /project/data/amoeba/Code/mysite/synteny/fasta-to-circos-log.txt")
    except:
        print("Error, fasta-to-circos.py not found. Is the etc folder with the script in it in /project/data/amoeba/Code/mysite/synteny?")
    
    try:
        os.system("module load mirna")
        print("Running circos. Please hold...")
        os.system("/project/soft/linux64/mirna/circos-0.69-4/bin/circos -conf /project/data/amoeba/Code/mysite/synteny/etc/circos.conf -silent -outputfile '/project/data/amoeba/Code/mysite/synteny/circosoutput' &> /project/data/amoeba/Code/mysite/synteny/circos-log.txt")
        print("circos run. Output is in /project/data/amoeba/Code/mysite/synteny/circos-log.txt")
    except:
        print("Error with circos, did you load module mirna? Did fasta-to-circos and miranda-to-circos both run?")
    
    #try:
     #   os.system("open etc/circosoutput.png")
    #except:
    #os.system("display circosoutput.png &")

        


    
if __name__ == "__main__":
	main(sys.argv)
