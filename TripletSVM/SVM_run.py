#!/bin/evn python
from __future__ import division
import re
import sys
import os
from math import *
###Takes the RNAfold output and filters out pseudo miRNAs. Returns a smaller version of the original input file.###


def main(argv):
    results_file = open("svm_filtered_results.fa","w")
    RNAfold_output= argv[1]
    triplet_svm_command = ("perl triplet_svm_classifier.pl "+RNAfold_output+ " RNAfold_svm_formatted.txt 20 > triplet_output.txt")#paste together the string to run the converter
    print("Starting. Please hold.")
    os.system(triplet_svm_command)#run the triplet filterer and converter
    print("Step 1/3 done.")
    path=os.getcwd()
    #svm_command = (path+"/svm-predict RNAfold_svm_formatted.txt training.model svm_results.txt")
    svm_command = ("svm-predict RNAfold_svm_formatted.txt training.model svm_results.txt > svm_output.txt")
    os.system(svm_command)#run the actual svm using the converted results
    print("Step 2/3 done.")

    unfiltered_results= open(argv[1],"r")
    unfiltered_results=unfiltered_results.read()
    unfiltered_results=unfiltered_results.split("\n")
    unfiltered_results=list(filter(None,unfiltered_results))#get rid of any empty elements
    
    only_RNA_results = open("1.txt","r")
    only_RNA_results = only_RNA_results.read()
    only_RNA_results = only_RNA_results.split("\n")
    only_RNA_results=list(filter(None,only_RNA_results))#get rid of any blank lines at the end
    
    stem_loops = open("2.txt","r")
    stem_loops = stem_loops.read()
    stem_loops = stem_loops.split("\n")
    stem_loops = list(filter(None,stem_loops))

    svm_results = open("svm_results.txt","r")#open the two files to compare them
    svm_results = svm_results.read()
    svm_results = svm_results.split("\n")
    svm_results = list(filter(None,svm_results))#remove any empties
    
    counter = 0
    for i in range(len(svm_results)):
        current_classifier= svm_results[i]
        if (current_classifier == "1"): #this is a true miRNA so find the corresponding entry in the stem loops
            current_stem_loop=stem_loops[((i+1)*3-3):((i+1)*3)]#extract the corresponding stem loop
            current_sl_seq=current_stem_loop[0]
            just_seq_num = re.findall(">\d*" ,current_sl_seq)#pulls out just the number of the sequence to allow us to refer back to the 1.txt temp file
            just_seq_num = just_seq_num[0]
            just_seq_num = int(just_seq_num[1:])#get just the number of the sequence without the >
            current_only_RNA =only_RNA_results[(just_seq_num*3-3):(just_seq_num*3)]#pull out the original result using the seq num as an index
            results_file.write(str(current_only_RNA[0])+"\n"+str(current_only_RNA[1])+"\n"+str(current_only_RNA[2]+"\n"))
            
        else:
            counter = counter +1
            pass#if there is anything other than 1 in the SVM results file then skip it
            
    
    results_file.close()
    print("Step 3/3 done.")
    print("Filtered successfully.")
    print(str(counter)+" pseudo miRNAs removed from your "+str(int(len(unfiltered_results))/3)+" inputs.")
    
    
if __name__ == "__main__":
	main(sys.argv)
